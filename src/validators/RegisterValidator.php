<?php


class RegisterValidator
{
    const MIN_NAME_LENGTH = 2;
    const MAX_NAME_LENGTH = 30;
    const MIN_LOGIN_LENGTH = 2;
    const MAX_LOGIN_LENGTH = 15;
    const MIN_SURNAME_LENGTH = 2;
    const MAX_SURNAME_LENGTH = 40;
    const MIN_PASSWORD_LENGTH = 8;
    const MAX_PASSWORD_LENGTH = 30;

    private $errors = array();

    function getErrors()
    {
        return $this->errors;
    }

    function resetErrors()
    {
        $this->errors = array();
    }

    function validateAll($login, $email, $password1, $password2, $name, $surname, $birthDate)
    {
        $result = $this->validateLogin($login);
        $result = $result && $this->validateEmail($email);
        $result = $result && $this->validatePassword($password1, $password2);
        $result = $result && $this->validateName($name);
        $result = $result && $this->validateSurname($surname);
        $result = $result && $this->validateBirthDate($birthDate);
        return $result;
    }

    private function validateLogin($login)
    {
        if($login === null or empty($login))
            $this->errors["login"][] = "Login can't be empty";
        else
        {
            if(strlen($login) < RegisterValidator::MIN_LOGIN_LENGTH || strlen($login) > RegisterValidator::MAX_LOGIN_LENGTH)
                $this->errors["login"][] = "Login must be between " . RegisterValidator::MIN_LOGIN_LENGTH . " and ". RegisterValidator::MAX_LOGIN_LENGTH . " characters length";
        }
        if(isset($this->errors["login"]) and count($this->errors["login"]) > 0)
            return false;
        else
            return true;
    }

    private function validateName($name)
    {
        if($name === null or empty($name))
            $this->errors["name"][] = "Name can't be empty";
        else
        {
            if(strlen($name) < RegisterValidator::MIN_NAME_LENGTH || strlen($name) > RegisterValidator::MAX_NAME_LENGTH)
                $this->errors["name"][] = "Name must be between " . RegisterValidator::MIN_NAME_LENGTH . " and ". RegisterValidator::MAX_NAME_LENGTH . " characters length";
        }
        if(isset($this->errors["name"]) and count($this->errors["name"]) > 0)
            return false;
        else
            return true;
    }

    private function validateSurname($surname)
    {
        if($surname === null or empty($surname))
            $this->errors["surname"][] = "Surname can't be empty";
        else
        {
            if(strlen($surname) < RegisterValidator::MIN_SURNAME_LENGTH || strlen($surname) > RegisterValidator::MAX_SURNAME_LENGTH)
                $this->errors["surname"][] = "Surname must be between " . RegisterValidator::MIN_SURNAME_LENGTH . " and ". RegisterValidator::MAX_SURNAME_LENGTH . " characters length";
        }
        if(isset($this->errors["surname"]) and count($this->errors["surname"]) > 0)
            return false;
        else
            return true;
    }

    private function validateEmail($email)
    {
        // temporary
        return true;

        if($email === null or empty($email))
            $this->errors["email"][] = "Email can't be empty";
        else
        {
            $email = preg_replace("/\(at\)/", "@", $email);
            if(!preg_match("/\@/", $email))
                $this->errors["email"][] = "Not valid email - missing '@' character";
        }
        if(isset($this->errors["email"]) and count($this->errors["email"]) > 0)
            return false;
        else
            return true;
    }

    private function validateBirthDate($birthDate)
    {
        if($birthDate === null or empty($birthDate))
            $this->errors["birthDate"][] = "Please provide your birth date";
        else
        {
            $datePieces = explode("-", $birthDate);
            $year = $datePieces[0];
            $month = $datePieces[1];
            $day = $datePieces[2];
            $currYear = date('Y');
            $currMonth = date('m');
            $currDay = date('d');
            if($year > $currYear)
            {
                $this->errors["birthDate"][] = "You were not born yet...";
            }
            if(strcmp($year, $currYear) == 0)
            {
                if($month > $currMonth)
                    $this->errors["birthDate"][] = "You were not born yet...";
                if($month == $currMonth)
                {
                    if($day > $currDay)
                        $this->errors["birthDate"][] = "You were not born yet...";
                }
            }
        }
        if(isset($this->errors["birthDate"]) and count($this->errors["birthDate"]) > 0)
            return false;
        else
            return true;
    }

    private function validatePassword($password1, $password2)
    {
        if($password1 != null and !empty($password1) and $password2 != null and !empty($password2))
        {
            if(strlen($password1) < RegisterValidator::MIN_PASSWORD_LENGTH or strlen($password1) > RegisterValidator::MAX_PASSWORD_LENGTH)
                $this->errors["password1"][] = "Password must be between ".RegisterValidator::MIN_PASSWORD_LENGTH." and ".
                    RegisterValidator::MAX_PASSWORD_LENGTH."characters";
            if($password1 != $password2)
                $this->errors["password2"][] = "Password repetition mismatch";
        }
        else if($password1 === null or empty($password1))
            $this->errors["password1"][] = "Password can't be empty";
        else if($password2 === null or empty($password2))
            $this->errors["password2"][] = "Password can't be empty";
        if(isset($this->errors["password1"]) and isset($this->errors["password2"]) and
            count($this->errors["password1"]) + count($this->errors["password2"]) > 0)
            return false;
        else
            return true;
    }

}

?>
