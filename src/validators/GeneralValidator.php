<?php

class GeneralValidator
{
    public function isEmpty($str)
    {
        return $str === null or strlen($str) == 0;
    }
}

?>
