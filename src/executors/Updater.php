<?php

require_once(__DIR__ . "/../service/UpdateService.php");
require_once(__DIR__ . "/../ConnectionWrapper.php");

class Updater
{
    /**
     * @param $what string
     * @param $id
     * @param mixed ...$params
     * @return false|string
     */
    public function update($what, $id, ...$params)
    {
        $result = array();
        $result["errors"] = array();

        try
        {
            $connectionWrapper = new \connection\ConnectionWrapper();
            $updateService = new UpdateService($connectionWrapper->getConnection());
            $updateService->$what($id, $params);
        } catch (Exception $e)
        {
            $result["errors"][] = $e->getMessage();
        }
        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}

?>
