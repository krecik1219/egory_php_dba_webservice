<?php

require_once(__DIR__ . "/../service/ComplexTaskService.php");
require_once(__DIR__ . "/../ConnectionWrapper.php");

class ComplexExecutor
{
    /**
     * @param $what string
     * @param $params
     * @return false|string
     */
    public function doComplexTask($what, ...$params)
    {
        $result = array();
        $result["errors"] = array();
        $result["taskResult"] = array();

        try
        {
            $connectionWrapper = new \connection\ConnectionWrapper();
            $complexTaskService = new ComplexTaskService($connectionWrapper->getConnection());
            $complexTaskService->$what($params);
            $result["taskResult"] = $complexTaskService->getTaskResult();

        } catch (Exception $e)
        {
            $result["errors"][] = $e->getMessage();
        }
        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}

?>
