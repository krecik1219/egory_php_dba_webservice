<?php

require_once(__DIR__ . "/../service/FetchService.php");
require_once(__DIR__ . "/../ConnectionWrapper.php");

class Fetcher
{
    private $encodeOptions = JSON_UNESCAPED_UNICODE;

    /**
     * @return int
     */
    public function getEncodeOptions()
    {
        return $this->encodeOptions;
    }

    /**
     * @param int $encodeOptions
     */
    public function setEncodeOptions($encodeOptions)
    {
        $this->encodeOptions = $encodeOptions;
    }



    public function fetch($what, $param = null)
    {
        $result = array();
        $result["errors"] = array();
        $result["fetchResult"] = array();

        try
        {
            $connectionWrapper = new \connection\ConnectionWrapper();
            $fetchService = new FetchService($connectionWrapper->getConnection());
            if($param == null)
                $fetchService->$what();
            else
                $fetchService->$what($param);
            $result["fetchResult"] = $fetchService->getFetchResult();

        } catch (Exception $e)
        {
            $result["errors"][] = $e->getMessage();
        }
        return json_encode($result, $this->encodeOptions);
    }
}

?>
