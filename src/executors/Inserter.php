<?php

require_once(__DIR__ . "/../service/InsertService.php");
require_once(__DIR__ . "/../ConnectionWrapper.php");

class Inserter
{
    /**
     * @param $what string
     * @param $params
     * @return false|string
     */
    public function insert($what, ...$params)
    {
        $result = array();
        $result["errors"] = array();
        $result["insertResult"] = array();

        try
        {
            $connectionWrapper = new \connection\ConnectionWrapper();
            $insertService = new InsertService($connectionWrapper->getConnection());
            $insertService->$what($params);
            $result["insertResult"] = $insertService->getInsertResult();

        } catch (Exception $e)
        {
            error_log("inserter exception caught: " . $e->getMessage());
            $result["errors"][] = $e->getMessage();
        }
        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}

?>
