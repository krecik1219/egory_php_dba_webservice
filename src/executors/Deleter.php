<?php

require_once(__DIR__ . "/../service/DeleteService.php");
require_once(__DIR__ . "/../ConnectionWrapper.php");

class Deleter
{
    /**
     * @param $what string
     * @param $id
     * @return false|string
     */
    public function delete($what, $id)
    {
        $result = array();
        $result["errors"] = array();

        try
        {
            $connectionWrapper = new \connection\ConnectionWrapper();
            $deleteService = new DeleteService($connectionWrapper->getConnection());
            $deleteService->$what($id);
        } catch (Exception $e)
        {
            $result["errors"][] = $e->getMessage();
        }
        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}

?>
