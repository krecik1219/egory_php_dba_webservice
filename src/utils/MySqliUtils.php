<?php

class MySqliUtils
{
    /**
     * @param $params
     * @return string
     * @throws Exception
     */
    public static function getTypesAbbreviations($params)
    {
        $paramsTypes = "";
        foreach($params as $param)
        {
            $paramsTypes .= MySqliUtils::getTypeAbbreviation(gettype($param));
        }
        return $paramsTypes;
    }

    /**
     * @param $type
     * @return string
     * @throws Exception
     */
    public static function getTypeAbbreviation($type)
    {
        switch($type)
        {
            case "string":
                return "s";
            case "integer":
                return "i";
            case "double":
                return "d";
            case "NULL":
                return "s";
            default:
             {
                 error_log("type error: " . print_r($type, true));
                 throw new Exception("Unsupported type");
             }
        }
    }
}

?>
