<?php

require_once(__DIR__ . "/../utils/MySqliUtils.php");
require_once(__DIR__ . "/../Globals.php");

class InsertService
{
    /**
     * @var mysqli
     */
    private $connection;
    private $insertResult;

    /**
     * InsertService constructor.
     * @param mysqli $connection
     */
    public function __construct(mysqli $connection)
    {
        $this->connection = $connection;
        $this->insertResult = array();
    }

    /**
     * @return array
     */
    public function getInsertResult()
    {
        return $this->insertResult;
    }

    /**
     * @param $params array
     * @throws Exception
     */
    public function insertTrip($params)
    {
        $query = "insert into trips(trip_id, trip_name, user_id, creation_date) ".
                 "values(NULL, ?, ?, NULL)";

        $this->insert($query, $params);
    }

    /**
     * @param $params array
     * @throws Exception
     */
    public function insertGotPath($params)
    {
        $query = "insert into got_paths(path_id, start_point_id, end_point_id, up_points, down_points) ".
            "values(NULL, ?, ?, ?, ?)";

        $this->insert($query, $params);
    }

    /**
     * @param $params array
     * @throws Exception
     */
    public function insertTripPath($params)
    {
        $query = "insert into trip_paths ".
                 "(trip_path_id, got_path_id, start_point_name, ".
                 "start_point_subrange_name, end_point_name, ".
                 "end_point_subrange_name, up_points, down_points, ".
                 "trip_id, path_order_number) ".
                 "values(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        $this->insert($query, $params);
    }

    /**
     * @param $params
     * @throws Exception
     */
    public function insertPhotoConfirmation($params)
    {
        $imageBase64String = $params[0];
        $photoConfirmationFiles = scandir(Globals::PHOTO_CONFIRMATIONS_DIR);
        $files = array_diff($photoConfirmationFiles, array('.', '..'));
        if(count($files) > 0)
        {
            $filesIds = array_map(function($file){
                return (int)explode(".", $file)[0];
            }, $files);
            $lastId = max($filesIds);
            $imageId = $lastId + 1;
        }
        else
            $imageId = 1;
        $image = base64_decode($imageBase64String);
        $fileName = $imageId . ".jpg";
        $filePath = Globals::PHOTO_CONFIRMATIONS_DIR . $fileName;
        if(!file_put_contents($filePath, $image))
            throw new Exception("Image PHOTO CONFIRMATION write error");

        $query = "insert into photo_confirmations ".
                 "(photo_confirmation_id, photo_url, trip_path_id) ".
                 "values(NULL, ?, ?)";

        $dbParams = [$fileName, $params[1]];
        $this->insert($query, $dbParams);
    }

    /**
     * @param $params
     * @throws Exception
     */
    public function insertPhotoTagToPoint($params)
    {
        $imageBase64String = $params[0];
        $photoTagsFiles = scandir(Globals::PHOTO_TAGS_DIR);
        $files = array_diff($photoTagsFiles, array('.', '..'));
        if(count($files) > 0)
        {
            $filesIds = array_map(function($file){
                return (int)explode(".", $file)[0];
            }, $files);
            $lastId = max($filesIds);
            $imageId = $lastId + 1;
        }
        else
            $imageId = 1;
        $image = base64_decode($imageBase64String);
        $fileName = $imageId . ".jpg";
        $filePath = Globals::PHOTO_TAGS_DIR . $fileName;
        if(!file_put_contents($filePath, $image))
            throw new Exception("Image PHOTO TAG write error");

        $query = "insert into photo_tags ".
            "(photo_tag_id, photo_url, location_point_id) ".
            "values(NULL, ?, ?)";

        $dbParams = [$fileName, $params[1]];
        $this->insert($query, $dbParams);
    }

    /**
     * @param $params array
     * @throws Exception
     */
    public function insertTrackingSession($params)
    {
        $query = "insert into tracking_sessions (session_id, session_name, user_id, start_datetime) ".
                 "values(null, ?, ?, ?)";

        $this->insert($query, $params);
    }

    /**
     * @param $params array
     * @throws Exception
     */
    public function insertLocationPointToTrackingSession($params)
    {
        $query = "insert into session_points(location_point_id, session_id, latitude, longitude, altitude, order_num) ".
                 "values(null, ?, ?, ?, ?, ?)";

        $this->insert($query, $params);
    }

    /**
     * @param $params
     * @throws Exception
     */
    public function insertUserBadge($params)
    {
        $query = "insert into user_badges (badge_id, user_id, award_date) ".
                 "values(?, ?, ?)";

        $this->insert($query, $params);
    }

    /**
     * @param $query
     * @param $params
     * @throws Exception
     */
    public function insert($query, $params)
    {
        $stmt = $this->connection->prepare($query);
        if(!$stmt)
            throw new Exception("Query error ".$this->connection->errno);
        $paramTypesAbbreviations = MySqliUtils::getTypesAbbreviations($params);
        if(!$stmt->bind_param($paramTypesAbbreviations, ... $params))
            throw new Exception("Parameters error ".$stmt->errno);
        if(!$stmt->execute())
            throw new Exception("Error executing query ".$stmt->errno);
        $stmt->close();
        $lastId = $this->connection->insert_id;
        $this->insertResult["lastId"] = $lastId;
    }
}

?>
