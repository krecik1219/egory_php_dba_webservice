<?php


class RegisterService
{
    /**
     * @var RegisterValidator
     */
    private $registerValidator;
    private $errors;
    private $registerResult;

    /**
     * @var mysqli
     */
    private $connection;

    /**
     * LoginService constructor.
     * @param $connection
     * @param $registerValidator
     */
    public function __construct($connection, $registerValidator)
    {
        $this->registerValidator = $registerValidator;
        $this->connection = $connection;
        $this->errors = array();
        $this->registerResult = array();
    }

    /**
     * @param $login
     * @param $email
     * @param $password1
     * @param $password2
     * @param $name
     * @param $surname
     * @param $birthDate
     * @return bool
     * @throws Exception
     */
    public function register($login, $email, $password1, $password2, $name, $surname, $birthDate)
    {
        $this->errors = array();
        if (!$this->registerValidator->validateAll($login, $email, $password1, $password2, $name, $surname, $birthDate))
        {
            $this->errors = $this->registerValidator->getErrors();
            return false;
        }
        $this->insertUserToDb($login, $email, $password1, $name, $surname, $birthDate);
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @return array
     */
    public function getRegisterResult(): array
    {
        return $this->registerResult;
    }

    /**
     * @param $login
     * @param $email
     * @param $password
     * @param $name
     * @param $surname
     * @param $birthDate
     * @return bool
     * @throws Exception
     */
    private function insertUserToDb($login, $email, $password, $name, $surname, $birthDate)
    {
        $this->errors = array();
        $query = "SELECT user_id FROM users WHERE email=? or login=?";
        $stmt = $this->connection->prepare($query);
        $stmt->bind_param("ss", $email, $login);
        if(!$stmt)
            throw new Exception("Query error " . $stmt->errno);
        $stmt->execute();
        if(!$stmt)
            throw new Exception("Query error " . $stmt->errno);
        $queryResult = $stmt->get_result();
        $stmt->close();
        if(!$queryResult)
            throw new Exception($this->connection->connect_errno);
        if($queryResult->num_rows > 0)
        {
            $this->errors["email"][] = "Email or login is already used";
            $this->errors["login"][] = "Email or login is already used";
            return false;
        }
        $queryResult->free_result();
        $query = "INSERT INTO users VALUES(NULL, ?, ?, ?, ?, ?, ?, 1)";
        $stmt = $this->connection->prepare($query);
        $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
        if(empty($mobileNum))
            $mobileNum = null;
        $stmt->bind_param("ssssss", $login, $hashedPassword, $email, $name, $surname, $birthDate);
        $stmt->execute();
        $this->registerResult["userId"] = $this->connection->insert_id;
    }
}

?>

