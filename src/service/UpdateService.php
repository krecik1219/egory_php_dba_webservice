<?php

require_once(__DIR__ . "/../utils/MySqliUtils.php");

class UpdateService
{
    /**
     * @var mysqli
     */
    private $connection;

    /**
     * InsertService constructor.
     * @param mysqli $connection
     */
    public function __construct(mysqli $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param $tripId
     * @param $params array
     * @throws Exception
     */
    public function updateTrip($tripId, $params)
    {
        $query = "update trips ".
                 "set trip_name = ? ".
                 "where trip_id = ?";

        $this->update($query, $tripId, $params);
    }

    /**
     * @param $pathId
     * @param $params
     * @throws Exception
     */
    public function updateGotPath($pathId, $params)
    {
        $query = "update got_paths ".
            "set up_points = ?, ".
            "down_points = ? ".
            "where path_id = ?";

        $this->update($query, $pathId, $params);
    }

    /**
     * @param $tripPathId
     * @param $params
     * @throws Exception
     */
    public function updateTripPath($tripPathId, $params) {
        $query = "update trip_paths ".
            "set is_done = ? ".
            "where trip_path_id = ?";

        $this->update($query, $tripPathId, $params);
    }

    /**
     * @param $sessionId
     * @param $params
     * @throws Exception
     */
    public function updateTrackingSession($sessionId, $params) {
        $query = "update tracking_sessions ".
            "set session_name = ?, ".
            "end_datetime = ? ".
            "where session_id = ?";

        $this->update($query, $sessionId, $params);
    }

    /**
     * @param $query
     * @param $id
     * @param $params
     * @throws Exception
     */
    public function update($query, $id, $params)
    {
        $stmt = $this->connection->prepare($query);
        if(!$stmt)
            throw new Exception("Query error ".$this->connection->errno);
        $idTypeAbbreviation = MySqliUtils::getTypeAbbreviation(gettype($id));
        $paramTypesAbbreviations = MySqliUtils::getTypesAbbreviations($params);
        $params[] = $id;
        if(!$stmt->bind_param($paramTypesAbbreviations.$idTypeAbbreviation, ...$params))
            throw new Exception("Parameters error ".$stmt->errno);
        if(!$stmt->execute())
            throw new Exception("Error executing query ".$stmt->errno);
        $stmt->close();
    }
}

?>
