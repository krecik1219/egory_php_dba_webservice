<?php

require_once(__DIR__ . "/../utils/MySqliUtils.php");

class DeleteService
{
    /**
     * @var mysqli
     */
    private $connection;

    /**
     * InsertService constructor.
     * @param mysqli $connection
     */
    public function __construct(mysqli $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param $tripId
     * @throws Exception
     */
    public function deleteTrip($tripId)
    {
        $query = "delete from trips where trip_id = ?";

        $this->delete($query, $tripId);
    }

    /**
     * @param $pathId
     * @throws Exception
     */
    public function deleteGotPath($pathId)
    {
        $query = "delete from got_paths where path_id = ?";

        $this->delete($query, $pathId);
    }

    /**
     * @param $tripPathId
     * @throws Exception
     */
    public function deleteTripPath($tripPathId)
    {
        $query = "delete from trip_paths where trip_path_id = ?";

        $this->delete($query, $tripPathId);
    }

    /**
     * @param $query
     * @param $id
     * @throws Exception
     */
    private function delete($query, $id)
    {
        $stmt = $this->connection->prepare($query);
        if(!$stmt)
            throw new Exception("Query error ".$this->connection->errno);
        $idTypeAbbreviation = MySqliUtils::getTypeAbbreviation(gettype($id));
        if(!$stmt->bind_param($idTypeAbbreviation, $id))
            throw new Exception("Parameters error ".$stmt->errno);
        if(!$stmt->execute())
            throw new Exception("Error executing query ".$stmt->errno);
        $stmt->close();
    }
}

?>
