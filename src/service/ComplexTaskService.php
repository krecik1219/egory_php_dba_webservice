<?php

require_once(__DIR__ . "/../utils/MySqliUtils.php");
require_once(__DIR__ . "/../Globals.php");
require_once(__DIR__ . "/../service/UpdateService.php");
require_once(__DIR__ . "/../service/InsertService.php");
require_once(__DIR__ . "/../service/FetchService.php");

class ComplexTaskService
{
    /**
     * @var mysqli
     */
    private $connection;
    private $taskResult;

    /**
     * InsertService constructor.
     * @param mysqli $connection
     */
    public function __construct(mysqli $connection)
    {
        $this->connection = $connection;
        $this->taskResult = array();
    }

    /**
     * @return array
     */
    public function getTaskResult()
    {
        return $this->taskResult;
    }

    /**
     * @param $params array
     * @throws Exception
     */
    public function insertTrip($params)
    {
        $query = "insert into trips(trip_id, trip_name, user_id, creation_date) ".
                 "values(NULL, ?, ?, NULL)";

        $this->insert($query, $params);
    }

    /**
     * @param $params
     * @throws Exception
     */
    public function syncTrackingSessions($params)
    {
        $userId = $params[0];
        $sessions = $params[1];
        $inNeedOfPointsSync = array();
        $inNeedOfFullInsertion = array();
        foreach($sessions as $session)
        {
            if((int) $session["sessionId"] != 0)
                $inNeedOfPointsSync[] = $session;
            else
                $inNeedOfFullInsertion[] = $session;
        }
        $this->connection->autocommit(false);
        try
        {
            error_log("starting");
            foreach ($inNeedOfPointsSync as $session)
            {
                error_log("proceeding inNeedOfPointSync: " . print_r($session, true));
                $this->updateSession($session);
                $this->insertSessionPoints($session["sessionId"], $session["trackingPoints"]);
            }
            foreach ($inNeedOfFullInsertion as $session)
            {
                error_log("proceeding inNeedOfFullInsertion: " . print_r($session, true));
                $sessionId = $this->insertSession($userId, $session);
                $this->insertSessionPoints($sessionId, $session["trackingPoints"]);
            }
            $result = $this->fetchAllUserSessions($userId);
            $this->connection->commit();
            $this->taskResult = $result;
            error_log("finished");
        }
        catch (Exception $e)
        {
            $this->connection->rollback();
            error_log("exception: " . $e->getMessage());
            throw $e;
        }
        finally
        {
            $this->connection->autocommit(true);
        }
    }

    /**
     * @param $session
     * @throws Exception
     */
    private function updateSession($session)
    {
        error_log("proceeding updateSession: " . print_r($session, true));
        $query = "update tracking_sessions ".
            "set end_datetime = ? ".
            "where session_id = ?";

        $params = array();
        $params[] = $session["endDatetime"] ?? null;
        $updateService = new UpdateService($this->connection);
        $updateService->update($query, $session["sessionId"], $params);
    }

    /**
     * @param $sessionId
     * @param $trackingPoints
     * @throws Exception
     */
    private function insertSessionPoints($sessionId, $trackingPoints)
    {
        error_log("insertSessionPoints: sessionId=" . $sessionId . " " . print_r($trackingPoints, true));
        if(count($trackingPoints) == 0)
            return;

        $params = array();
        $query = "insert into session_points(location_point_id, session_id, latitude, longitude, altitude, order_num) values ";
        foreach ($trackingPoints as $point)
        {
            $query .= "(null, ?, ?, ?, ?, ?), ";
            $params[] = $sessionId;
            $params[] = $point["latitude"];
            $params[] = $point["longitude"];
            $params[] = $point["altitude"];
            $params[] = $point["orderNum"];
        }
        $query = substr($query, 0, strlen($query) - 2);

        error_log("query: " . $query);

        $insertService = new InsertService($this->connection);
        $insertService->insert($query, $params);
    }

    /**
     * @param $userId
     * @param $session
     * @return int
     * @throws Exception
     */
    private function insertSession($userId, $session)
    {
        error_log("insertSession: userId=" . $userId . " " . print_r($session, true));
        $params = array();
        $params[] = $session["sessionName"];
        $params[] = $userId;
        $params[] = $session["startDatetime"];
        $params[] = $session["endDatetime"] ?? null;

        $query = "insert into tracking_sessions (session_id, session_name, user_id, start_datetime, end_datetime) ".
            "values(null, ?, ?, ?, ?)";

        $insertService = new InsertService($this->connection);
        $insertService->insert($query, $params);
        return $insertService->getInsertResult()["lastId"];
    }

    /**
     * @param $userId
     * @return array
     * @throws Exception
     */
    private function fetchAllUserSessions($userId)
    {
        error_log("insertSession: fetchAllUserSessions");
        $fetchService = new FetchService($this->connection);
        $fetchService->fetchTrackingSessionsByUserId($userId);
        return $fetchService->getFetchResult();
    }
}

?>
