<?php

require_once(__DIR__ . "/../Globals.php");
require_once(__DIR__ . "/../utils/ArrayUtils.php");

class FetchService
{
    /**
     * @var mysqli
     */
    private $connection;
    private $fetchResult;
    /**
     * FetchService constructor.
     * @param mysqli $connection
     */
    public function __construct(mysqli $connection)
    {
        $this->connection = $connection;
        $this->fetchResult = array();
    }

    public function getFetchResult()
    {
        return $this->fetchResult;
    }

    /**
     * @throws Exception
     */
    public function fetchMountainPoints()
    {
        $query = "select point_id, point_name, mp.subrange_id, latitude, longitude, altitude, ".
                 "subrange_name, ms.range_id, range_name ".
                 "from mountain_points mp ".
                 "join mountain_subranges ms on mp.subrange_id = ms.subrange_id ".
                 "join mountain_ranges mr on ms.range_id = mr.range_id";
        $stmt = $this->connection->prepare($query);
        if(!$stmt)
            throw new Exception("Query error ".$this->connection->errno);
        $this->doFetchAll($stmt);
    }

    /**
     * @throws Exception
     */
    public function fetchMountainRanges()
    {
        $query = "select * from mountain_ranges";
        $stmt = $this->connection->prepare($query);
        if(!$stmt)
            throw new Exception("Query error ".$this->connection->errno);
        $this->doFetchAll($stmt);
    }

    /**
     * @throws Exception
     */
    public function fetchMountainSubranges()
    {
        $query = $query = "select subrange_id, subrange_name, ms.range_id, range_name ".
                          "from mountain_subranges ms ".
                          "join mountain_ranges mr on ms.range_id = mr.range_id";
        $stmt = $this->connection->prepare($query);
        if(!$stmt)
            throw new Exception("Query error ".$this->connection->errno);
        $this->doFetchAll($stmt);
    }

    /**
     * @param $rangeId
     * @throws Exception
     */
    public function fetchMountainSubrangesByRangeId($rangeId)
    {
        $query = "select subrange_id, subrange_name, ms.range_id, range_name ".
                 "from mountain_subranges ms ".
                 "join mountain_ranges mr on mr.range_id = ? and ms.range_id = mr.range_id";
        $stmt = $this->connection->prepare($query);
        if(!$stmt)
            throw new Exception("Query error ".$this->connection->errno);
        if(!$stmt->bind_param("d", $rangeId))
            throw new Exception("Parameters error ".$stmt->errno);
        $this->doFetchAll($stmt);
    }

    /**
     * @param $subrangeId
     * @throws Exception
     */
    public function fetchGotPathsBySubrangeId($subrangeId)
    {
        $query = 'SELECT path_id, start_point_id, mp1.point_name "start_point_name", mp1.latitude "start_point_latitude", '.
                 'mp1.longitude "start_point_longitude", mp1.altitude "start_point_altitude", '.
                 'ms1.subrange_id "start_point_subrange_id", ms1.subrange_name "start_point_subrange_name", '.
                 'ms1.range_id "start_point_range_id", mr1.range_name "start_point_range_name", '.
                 'ms2.subrange_id "end_point_subrange_id", ms2.subrange_name "end_point_subrange_name", '.
                 'ms2.range_id "end_point_range_id", mr2.range_name "end_point_range_name", '.
                 'end_point_id, mp2.point_name "end_point_name", mp2.latitude "end_point_latitude", '.
                 'mp2.longitude "end_point_longitude", mp2.altitude "end_point_altitude", up_points, down_points '.
                 'FROM got_paths gp '.
                 'join mountain_points mp1 on gp.start_point_id = mp1.point_id '.
                 'join mountain_points mp2 on gp.end_point_id = mp2.point_id '.
                 'join mountain_subranges ms1 on mp1.subrange_id = ms1.subrange_id '.
                 'join mountain_subranges ms2 on mp2.subrange_id = ms2.subrange_id '.
                 'join mountain_ranges mr1 on ms1.range_id = mr1.range_id '.
                 'join mountain_ranges mr2 on ms2.range_id = mr2.range_id '.
                 'where mp2.subrange_id = ?;';
        $stmt = $this->connection->prepare($query);
        if(!$stmt)
            throw new Exception("Query error ".$this->connection->errno);
        if(!$stmt->bind_param("d", $subrangeId))
            throw new Exception("Parameters error ".$stmt->errno);
        $this->doFetchAll($stmt);
    }

    /**
     * @param $startPointName
     * @throws Exception
     */
    public function fetchGotPathsByStartPointName($startPointName)
    {
        $query = 'SELECT path_id, start_point_id, mp1.point_name "start_point_name", mp1.latitude "start_point_latitude", '.
                 'mp1.longitude "start_point_longitude", mp1.altitude "start_point_altitude", '.
                 'ms1.subrange_id "start_point_subrange_id", ms1.subrange_name "start_point_subrange_name", '.
                 'ms1.range_id "start_point_range_id", mr1.range_name "start_point_range_name", '.
                 'ms2.subrange_id "end_point_subrange_id", ms2.subrange_name "end_point_subrange_name", '.
                 'ms2.range_id "end_point_range_id", mr2.range_name "end_point_range_name", '.
                 'end_point_id, mp2.point_name "end_point_name", mp2.latitude "end_point_latitude", '.
                 'mp2.longitude "end_point_longitude", mp2.altitude "end_point_altitude", up_points, down_points '.
                 'FROM got_paths gp '.
                 'join mountain_points mp1 on gp.start_point_id = mp1.point_id '.
                 'join mountain_points mp2 on gp.end_point_id = mp2.point_id '.
                 'join mountain_subranges ms1 on mp1.subrange_id = ms1.subrange_id '.
                 'join mountain_subranges ms2 on mp2.subrange_id = ms2.subrange_id '.
                 'join mountain_ranges mr1 on ms1.range_id = mr1.range_id '.
                 'join mountain_ranges mr2 on ms2.range_id = mr2.range_id '.
                 'where mp1.point_name = ?;';
        $stmt = $this->connection->prepare($query);
        if(!$stmt)
            throw new Exception("Query error ".$this->connection->errno);
        if(!$stmt->bind_param("s", $startPointName))
            throw new Exception("Parameters error ".$stmt->errno);
        $this->doFetchAll($stmt);
    }

    /**
     * @param $endPointName
     * @throws Exception
     */
    public function fetchGotPathsByEndPointName($endPointName)
    {
        $query = 'SELECT path_id, start_point_id, mp1.point_name "start_point_name", mp1.latitude "start_point_latitude", '.
            'mp1.longitude "start_point_longitude", mp1.altitude "start_point_altitude", '.
            'ms1.subrange_id "start_point_subrange_id", ms1.subrange_name "start_point_subrange_name", '.
            'ms1.range_id "start_point_range_id", mr1.range_name "start_point_range_name", '.
            'ms2.subrange_id "end_point_subrange_id", ms2.subrange_name "end_point_subrange_name", '.
            'ms2.range_id "end_point_range_id", mr2.range_name "end_point_range_name", '.
            'end_point_id, mp2.point_name "end_point_name", mp2.latitude "end_point_latitude", '.
            'mp2.longitude "end_point_longitude", mp2.altitude "end_point_altitude", up_points, down_points '.
            'FROM got_paths gp '.
            'join mountain_points mp1 on gp.start_point_id = mp1.point_id '.
            'join mountain_points mp2 on gp.end_point_id = mp2.point_id '.
            'join mountain_subranges ms1 on mp1.subrange_id = ms1.subrange_id '.
            'join mountain_subranges ms2 on mp2.subrange_id = ms2.subrange_id '.
            'join mountain_ranges mr1 on ms1.range_id = mr1.range_id '.
            'join mountain_ranges mr2 on ms2.range_id = mr2.range_id '.
            'where mp2.point_name = ?;';
        $stmt = $this->connection->prepare($query);
        if(!$stmt)
            throw new Exception("Query error ".$this->connection->errno);
        if(!$stmt->bind_param("s", $endPointName))
            throw new Exception("Parameters error ".$stmt->errno);
        $this->doFetchAll($stmt);
    }

    /**
     * @param $userId
     * @throws Exception
     */
    public function fetchTripsByUserId($userId)
    {
        $query = "SELECT trip_id, trip_name, user_id, creation_date, ".
                 "t.trip_status_id, trip_status_name FROM trips t ".
                 "join trip_statuses ts on t.trip_status_id = ts.trip_status_id WHERE user_id = ? ".
                 "order by creation_date desc";
        $stmt = $this->connection->prepare($query);
        if(!$stmt)
            throw new Exception("Query error ".$this->connection->errno);
        if(!$stmt->bind_param("d", $userId))
            throw new Exception("Parameters error ".$stmt->errno);
        $this->doFetchAll($stmt);
    }

    /**
     * @param $tripId
     * @throws Exception
     */
    public function fetchTripPathsByTripId($tripId)
    {
        $query = "SELECT * FROM trip_paths WHERE trip_id = ? order by path_order_number";
        $stmt = $this->connection->prepare($query);
        if(!$stmt)
            throw new Exception("Query error ".$this->connection->errno);
        if(!$stmt->bind_param("d", $tripId))
            throw new Exception("Parameters error ".$stmt->errno);
        $this->doFetchAll($stmt);
    }

    /**
     * @param $startPointId
     * @throws Exception
     */
    public function fetchEndPointsBasedOnStartPoint($startPointId)
    {
        $query = "SELECT gp.end_point_id, mp.point_name, mp.longitude, mp.latitude, mp.altitude, ".
                 "mp.subrange_id, sr.subrange_name, r.range_id, r.range_name ".
                 "FROM got_paths gp ".
                 "join mountain_points mp on gp.end_point_id = mp.point_id ".
                 "join mountain_subranges sr on mp.subrange_id = sr.subrange_id ".
                 "join mountain_ranges r on sr.range_id = r.range_id ".
                 "WHERE gp.start_point_id = ?";
        $stmt = $this->connection->prepare($query);
        if(!$stmt)
            throw new Exception("Query error ".$this->connection->errno);
        if(!$stmt->bind_param("d", $startPointId))
            throw new Exception("Parameters error ".$stmt->errno);
        $this->doFetchAll($stmt);
    }

    /**
     * @param $endPointId
     * @throws Exception
     */
    public function fetchStartPointsBasedOnEndPoint($endPointId)
    {
        $query = "SELECT gp.start_point_id, mp.point_name, mp.latitude, mp.longitude, mp.altitude, ".
                 "mp.subrange_id, sr.subrange_name, r.range_id, r.range_name ".
                 "FROM got_paths gp ".
                 "join mountain_points mp on gp.start_point_id = mp.point_id ".
                 "join mountain_subranges sr on mp.subrange_id = sr.subrange_id ".
                 "join mountain_ranges r on sr.range_id = r.range_id ".
                 "WHERE gp.end_point_id = ?";
        $stmt = $this->connection->prepare($query);
        if(!$stmt)
            throw new Exception("Query error ".$this->connection->errno);
        if(!$stmt->bind_param("d", $endPointId))
            throw new Exception("Parameters error ".$stmt->errno);
        $this->doFetchAll($stmt);
    }

    /**
     * @param $subrangeId
     * @throws Exception
     */
    public function fetchPointsBasedOnSubrange($subrangeId)
    {
        $query = "select point_id, point_name, mp.latitude, mp.longitude, mp.altitude, ".
                 "mp.subrange_id, subrange_name, ms.range_id, range_name ".
                 "from mountain_points mp ".
                 "join mountain_subranges ms on mp.subrange_id = ? and mp.subrange_id = ms.subrange_id ".
                 "join mountain_ranges mr on ms.range_id = mr.range_id";
        $stmt = $this->connection->prepare($query);
        if(!$stmt)
            throw new Exception("Query error ".$this->connection->errno);
        if(!$stmt->bind_param("d", $subrangeId))
            throw new Exception("Parameters error ".$stmt->errno);
        $this->doFetchAll($stmt);
    }

    /**
     * @param $tripPathId
     * @throws Exception
     */
    public function fetchPhotoConfirmationsBasedOnTripPath($tripPathId)
    {
        $query = "SELECT * FROM photo_confirmations WHERE trip_path_id = ?";
        $stmt = $this->connection->prepare($query);
        if(!$stmt)
            throw new Exception("Query error ".$this->connection->errno);
        if(!$stmt->bind_param("d", $tripPathId))
            throw new Exception("Parameters error ".$stmt->errno);
        $this->doFetchAll($stmt);
        $photos = array();
        foreach($this->fetchResult as $confirmation)
        {
            $image = file_get_contents(Globals::PHOTO_CONFIRMATIONS_DIR . $confirmation["photo_url"]);
            $base64 = base64_encode($image);
            $photos[] = $base64;
        }
        $result = array();
        $counter = 0;
        foreach($this->fetchResult as $confirmation)
        {
            $confirmationWithImage = array();
            $confirmationWithImage["photo_confirmation_id"] = $confirmation["photo_confirmation_id"];
            $confirmationWithImage["image"] = $photos[$counter];
            $counter++;
            $confirmationWithImage["trip_path_id"] = $confirmation["trip_path_id"];
            $result[] = $confirmationWithImage;
        }
        $this->fetchResult = $result;
    }

    /**
     * @param $userId
     * @throws Exception
     */
    public function fetchTrackingSessionsByUserId($userId)
    {
        $query = "select ts.session_id, ts.session_name, ".
                 "ts.start_datetime, ts.end_datetime, ".
                 "sp.location_point_id, sp.latitude, sp.longitude, sp.altitude, sp.order_num, ".
                 "pt.photo_tag_id, pt.photo_url ".
	             "from users u join tracking_sessions ts ".
    	         "on ts.user_id = ? ".
                 "left join session_points sp ".
    	         "on sp.session_id = ts.session_id ".
                 "left join photo_tags pt ".
                 "on sp.location_point_id = pt.location_point_id ".
                 "order by ts.session_id, sp.order_num, pt.photo_tag_id";
        $stmt = $this->connection->prepare($query);
        if(!$stmt)
            throw new Exception("Query error ".$this->connection->errno);
        if(!$stmt->bind_param("d", $userId))
            throw new Exception("Parameters error ".$stmt->errno);
        $this->doFetchAll($stmt);
        $result = array();
        foreach($this->fetchResult as $tp)
        {
            if(!ArrayUtils::arrayExists($result, function(array $elem) use ($tp) {return $elem["session_id"] == $tp["session_id"];}))
            {
                $auxArray = array();
                $auxArray["session_id"] = $tp["session_id"];
                $auxArray["session_name"] = $tp["session_name"];
                $auxArray["start_datetime"] = $tp["start_datetime"];
                $auxArray["end_datetime"] = $tp["end_datetime"];
                $auxArray["tracking_points"] = array();
                $result[] = $auxArray;
            }
            $resultsLastIndex = count($result) - 1;
            if($tp["location_point_id"] != null &&
                !ArrayUtils::arrayExists(
                    $result[$resultsLastIndex]["tracking_points"],
                    function(array $elem) use ($tp) {return $elem["location_point_id"] == $tp["location_point_id"];}))
            {
                $auxArray = array();
                $auxArray["location_point_id"] = $tp["location_point_id"];
                $auxArray["latitude"] = $tp["latitude"];
                $auxArray["longitude"] = $tp["longitude"];
                $auxArray["altitude"] = $tp["altitude"];
                $auxArray["order_num"] = $tp["order_num"];
                $auxArray["photo_tags"] = array();
                $result[$resultsLastIndex]["tracking_points"][] = $auxArray;
            }

            if($tp["location_point_id"] == null)
            {
                $result[$resultsLastIndex]["tracking_points"] = array();
            }

            $trackingPointsLastIndex = count($result[$resultsLastIndex]["tracking_points"]) - 1;
            if($tp["photo_tag_id"] != null)
            {
                $auxArray = array();
                $auxArray["photo_tag_id"] = $tp["photo_tag_id"];
                $image = file_get_contents(Globals::PHOTO_TAGS_DIR . $tp["photo_url"]);
                $base64 = base64_encode($image);
                $auxArray["image"] = $base64;
                $result[$resultsLastIndex]["tracking_points"][$trackingPointsLastIndex]["photo_tags"][] = $auxArray;
            }
            else
            {
                if($trackingPointsLastIndex + 1 > 0)
                {
                    $result[$resultsLastIndex]["tracking_points"][$trackingPointsLastIndex] ["photo_tags"] = array();
                }
            }
        }
        $this->fetchResult = $result;
    }

    /**
     * @param $userId
     * @throws Exception
     */
    public function fetchBadgesByUserId($userId)
    {
        $query = "SELECT ub.badge_id, gb.badge_name, gb.required_points, ub.award_date ".
                 "FROM user_badges ub join got_badges gb ".
                 "on ub.badge_id = gb.badge_id and ub.user_id = ?";
        $stmt = $this->connection->prepare($query);
        if(!$stmt)
            throw new Exception("Query error ".$this->connection->errno);
        if(!$stmt->bind_param("d", $userId))
            throw new Exception("Parameters error ".$stmt->errno);
        $this->doFetchAll($stmt);
    }

    /**
     * @param mysqli_stmt $stmt
     * @throws Exception
     */
    private function doFetchAll($stmt)
    {
        $this->fetchResult = array();
        if(!$stmt->execute())
            throw new Exception("Error executing query ".$stmt->errno);
        $result = $stmt->get_result();
        $stmt->close();
        if(!$result)
            throw new Exception($this->connection->connect_errno);

        // ini_set("log_errors", 1);
        // ini_set("error_log", "/tmp/php-error.log");
        while($row = $result->fetch_assoc())
        {
            $this->fetchResult[] = $row;
            // error_log('item obj: '.$items[0]->getName().' price: '.$items[0]->getPrice());
        }
    }
}

?>
