<?php

require_once(__DIR__ . "/../utils/StringUtils.php");
require_once (__DIR__ . "/../validators/GeneralValidator.php");

class LoginService
{
    /**
     * @var GeneralValidator
     */
    private $generalValidator;
    private $errors;
    private $loginResult;

    /**
     * @var mysqli
     */
    private $connection;

    /**
     * LoginService constructor.
     * @param $connection
     * @param $generalValidator
     */
    public function __construct($connection, $generalValidator)
    {
        $this->generalValidator = $generalValidator;
        $this->connection = $connection;
        $this->errors = array();
        $this->loginResult = array();
    }

    /**
     * @param $login
     * @param $email
     * @param $password
     * @return bool
     * @throws Exception
     */
    public function login($login, $email, $password)
    {
        $this->errors = array();
        $this->loginResult = array();

        $isLoggingByEmail = StringUtils::isEmail($email);
        if($this->generalValidator->isEmpty($email) && $this->generalValidator->isEmpty($login))
        {
            $this->errors["email"][] = "Email or login must be provided";
            $this->errors["login"][] = "Email or login must be provided";
        }
        if($this->generalValidator->isEmpty($password))
            $this->errors["password"][] = "Password can't be empty";
        if(count($this->errors) > 0)
            return false;

        if($isLoggingByEmail)
        {
            $email = htmlentities($email, ENT_QUOTES, "UTF-8");
            $stmt = $this->prepareLoginQueryWithEmail($email);
        }
        else
        {
            $login = htmlentities($login, ENT_QUOTES, "UTF-8");
            $stmt = $this->prepareLoginQueryWithLogin($login);
        }

        if(!$stmt->execute())
            throw new Exception("Error executing query ".$stmt->errno);

        $queryResult = $stmt->get_result();
        $stmt->close();
        $usersNumber = $queryResult->num_rows;
        if($usersNumber <= 0)
        {
            $this->errors["logging"][] = "Wrong credentials";
            return false;
        }
        $row = $queryResult->fetch_assoc();

        if(password_verify($password, $row['password']))
        {
            $this->loginResult["user_id"] = $row["user_id"];
            $this->loginResult["login"] = $row["login"];
            $this->loginResult["name"] = $row["name"];
            $this->loginResult["surname"] = $row["surname"];
            $this->loginResult["user_type_name"] = $row["type_name"];
            $queryResult->free_result();
            return true;
        }
        else
        {
            $queryResult->free_result();
            $this->errors["logging"][] = "Wrong credentials";
            return false;
        }
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getLoginResult()
    {
        return $this->loginResult;
    }

    /**
     * @param $login
     * @return mysqli_stmt
     * @throws Exception
     */
    private function prepareLoginQueryWithLogin($login)
    {
        $sqlQuery = "SELECT user_id, login, password, name, surname, type_name FROM users u join user_types ut on u.user_type_id = ut.user_type_id WHERE login=?";
        return $this->prepareQuery($sqlQuery, $login);
    }

    /**
     * @param $email
     * @return mysqli_stmt
     * @throws Exception
     */
    private function prepareLoginQueryWithEmail($email)
    {
        $sqlQuery = "SELECT user_id, login, password, name, surname, type_name FROM users u join user_types ut on u.user_type_id = ut.user_type_id WHERE email=?";
        return $this->prepareQuery($sqlQuery, $email);
    }

    private function prepareQuery($sqlQuery, $param)
    {
        $stmt = $this->connection->prepare($sqlQuery);
        if(!$stmt)
            throw new Exception("Query error");
        if(!$stmt->bind_param("s", $param))
            throw new Exception("Query error".$stmt->errno);
        return $stmt;
    }
}

?>
