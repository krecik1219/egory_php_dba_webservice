<?php
if($_SERVER["REQUEST_METHOD"] == "GET")
{
    require_once(__DIR__ . '/../../src/executors/Fetcher.php');

    $endPointId = $_GET["endPointId"];

    $fetcher = new Fetcher();
    echo $fetcher->fetch("fetchStartPointsBasedOnEndPoint", $endPointId);
}
?>
