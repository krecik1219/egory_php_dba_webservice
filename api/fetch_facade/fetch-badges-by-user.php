<?php
if($_SERVER["REQUEST_METHOD"] == "GET")
{
    require_once(__DIR__ . '/../../src/executors/Fetcher.php');

    $userId = $_GET["userId"];

    $fetcher = new Fetcher();
    echo $fetcher->fetch("fetchBadgesByUserId", $userId);
}
?>
