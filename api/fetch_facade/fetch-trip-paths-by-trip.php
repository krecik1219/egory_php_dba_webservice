<?php
if($_SERVER["REQUEST_METHOD"] == "GET")
{
    require_once(__DIR__ . '/../../src/executors/Fetcher.php');

    $tripId = $_GET["tripId"];

    $fetcher = new Fetcher();
    echo $fetcher->fetch("fetchTripPathsByTripId", $tripId);
}
?>
