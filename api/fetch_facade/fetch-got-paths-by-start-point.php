<?php
if($_SERVER["REQUEST_METHOD"] == "GET")
{
    require_once(__DIR__ . '/../../src/executors/Fetcher.php');

    $startPointName = $_GET["startPointName"];

    $fetcher = new Fetcher();
    echo $fetcher->fetch("fetchGotPathsByStartPointName", $startPointName);
}
?>
