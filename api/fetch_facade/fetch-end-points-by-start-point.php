<?php
if($_SERVER["REQUEST_METHOD"] == "GET")
{
    require_once(__DIR__ . '/../../src/executors/Fetcher.php');

    $startPointId = $_GET["startPointId"];

    $fetcher = new Fetcher();
    echo $fetcher->fetch("fetchEndPointsBasedOnStartPoint", $startPointId);
}
?>
