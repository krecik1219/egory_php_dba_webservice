<?php
if($_SERVER["REQUEST_METHOD"] == "GET")
{
    require_once(__DIR__ . '/../../src/executors/Fetcher.php');

    $tripPathId = $_GET["tripPathId"];

    $fetcher = new Fetcher();
    $fetcher->setEncodeOptions(JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    echo $fetcher->fetch("fetchPhotoConfirmationsBasedOnTripPath", $tripPathId);
}
?>
