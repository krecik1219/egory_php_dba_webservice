<?php
if($_SERVER["REQUEST_METHOD"] == "GET")
{
    require_once(__DIR__ . '/../../src/executors/Fetcher.php');

    $rangeId = $_GET["rangeId"];

    $fetcher = new Fetcher();
    echo $fetcher->fetch("fetchMountainSubrangesByRangeId", $rangeId);
}
?>
