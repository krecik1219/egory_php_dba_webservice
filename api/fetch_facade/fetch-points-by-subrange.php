<?php
if($_SERVER["REQUEST_METHOD"] == "GET")
{
    require_once(__DIR__ . '/../../src/executors/Fetcher.php');

    $subrangeId = $_GET["subrangeId"];

    $fetcher = new Fetcher();
    echo $fetcher->fetch("fetchPointsBasedOnSubrange", $subrangeId);
}
?>
