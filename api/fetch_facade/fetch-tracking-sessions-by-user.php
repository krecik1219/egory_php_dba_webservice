<?php
if($_SERVER["REQUEST_METHOD"] == "GET")
{
    require_once(__DIR__ . '/../../src/executors/Fetcher.php');

    $userId = $_GET["userId"];

    $fetcher = new Fetcher();
    $fetcher->setEncodeOptions(JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    $res = $fetcher->fetch("fetchTrackingSessionsByUserId", $userId);
    echo $res;
}
?>
