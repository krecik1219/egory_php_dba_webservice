<?php
if($_SERVER["REQUEST_METHOD"] == "GET")
{
    require_once(__DIR__ . '/../../src/executors/Fetcher.php');

    $endPointName = $_GET["endPointName"];

    $fetcher = new Fetcher();
    echo $fetcher->fetch("fetchGotPathsByEndPointName", $endPointName);
}
?>
