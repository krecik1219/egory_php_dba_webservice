<?php
if($_SERVER["REQUEST_METHOD"] == "POST")
{
    require_once(__DIR__ . '/../../src/executors/Inserter.php');

    $jsonRequestParams = json_decode(file_get_contents('php://input'), true);

    $image = $jsonRequestParams["image"];
    $locationPointId = (int)$jsonRequestParams["locationPointId"];

    $inserter = new Inserter();
    echo $inserter->insert("insertPhotoTagToPoint", $image, $locationPointId);
}
?>
