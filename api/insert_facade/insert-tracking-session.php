<?php
if($_SERVER["REQUEST_METHOD"] == "POST")
{
    require_once(__DIR__ . '/../../src/executors/Inserter.php');

    $jsonRequestParams = json_decode(file_get_contents('php://input'), true);

    $userId = (int)$jsonRequestParams["userId"];
    $sessionName = $jsonRequestParams["sessionName"] ?? null;
    $startDatetime = $jsonRequestParams["startDatetime"];

    $inserter = new Inserter();
    echo $inserter->insert("insertTrackingSession", $sessionName, $userId, $startDatetime);
}
?>
