<?php
if($_SERVER["REQUEST_METHOD"] == "POST")
{
    require_once(__DIR__ . '/../../src/executors/Inserter.php');

    $jsonRequestParams = json_decode(file_get_contents('php://input'), true);

    $gotPathId = (int)$jsonRequestParams["gotPathId"];
    $startPointName = $jsonRequestParams["startPointName"];
    $startPointSubrangeName = $jsonRequestParams["startPointSubrangeName"];
    $endPointName = $jsonRequestParams["endPointName"];
    $endPointSubrangeName = $jsonRequestParams["endPointSubrangeName"];
    $upPoints = (int)$jsonRequestParams["upPoints"];
    $downPoints = (int)$jsonRequestParams["downPoints"];
    $tripId = (int)$jsonRequestParams["tripId"];
    $pathOrderNumber = (int)$jsonRequestParams["pathOrderNumber"];

    $inserter = new Inserter();
    echo $inserter->insert("insertTripPath",
                           $gotPathId,
                           $startPointName,
                           $startPointSubrangeName,
                           $endPointName,
                           $endPointSubrangeName,
                           $upPoints,
                           $downPoints,
                           $tripId,
                           $pathOrderNumber);
}
?>
