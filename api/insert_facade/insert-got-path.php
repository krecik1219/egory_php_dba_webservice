<?php
if($_SERVER["REQUEST_METHOD"] == "POST")
{
    require_once(__DIR__ . '/../../src/executors/Inserter.php');

    $jsonRequestParams = json_decode(file_get_contents('php://input'), true);

    $startPointId = (int)$jsonRequestParams["startPointId"];
    $endPointId = (int)$jsonRequestParams["endPointId"];
    $upPoints = (int)$jsonRequestParams["upPoints"];
    $downPoints = (int)$jsonRequestParams["downPoints"];

    $inserter = new Inserter();
    echo $inserter->insert("insertGotPath", $startPointId, $endPointId, $upPoints, $downPoints);
}
?>
