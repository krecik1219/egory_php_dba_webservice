<?php
if($_SERVER["REQUEST_METHOD"] == "POST")
{
    require_once(__DIR__ . '/../../src/executors/Inserter.php');

    $jsonRequestParams = json_decode(file_get_contents('php://input'), true);

    $sessionId = (int)$jsonRequestParams["sessionId"];
    $latitude = (double) $jsonRequestParams["latitude"];
    $longitude = (double) $jsonRequestParams["longitude"];
    $altitude = (double) $jsonRequestParams["altitude"];
    $orderNum = (int) $jsonRequestParams["orderNum"];

    $inserter = new Inserter();
    echo $inserter->insert("insertLocationPointToTrackingSession", $sessionId, $latitude, $longitude, $altitude, $orderNum);
}
?>
