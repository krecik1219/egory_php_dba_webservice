<?php
if($_SERVER["REQUEST_METHOD"] == "POST")
{
    require_once(__DIR__ . '/../../src/executors/Inserter.php');

    $jsonRequestParams = json_decode(file_get_contents('php://input'), true);

    $badgeId = $jsonRequestParams["badgeId"];
    $awardDate = $jsonRequestParams["awardDate"];
    $userId = (int)$jsonRequestParams["userId"];

    $inserter = new Inserter();
    echo $inserter->insert("insertUserBadge", $badgeId, $userId, $awardDate);
}
?>
