<?php
if($_SERVER["REQUEST_METHOD"] == "POST")
{
    require_once(__DIR__ . '/../../src/executors/Inserter.php');

    $jsonRequestParams = json_decode(file_get_contents('php://input'), true);

    $tripName = $jsonRequestParams["tripName"];
    $userId = (int)$jsonRequestParams["userId"];

    $inserter = new Inserter();
    echo $inserter->insert("insertTrip", $tripName, $userId);
}
?>
