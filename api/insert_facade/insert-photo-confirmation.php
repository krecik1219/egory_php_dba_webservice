<?php
if($_SERVER["REQUEST_METHOD"] == "POST")
{
    require_once(__DIR__ . '/../../src/executors/Inserter.php');

    $jsonRequestParams = json_decode(file_get_contents('php://input'), true);

    $image = $jsonRequestParams["image"];
    $tripPathId = (int)$jsonRequestParams["tripPathId"];

    $inserter = new Inserter();
    echo $inserter->insert("insertPhotoConfirmation", $image, $tripPathId);
}
?>
