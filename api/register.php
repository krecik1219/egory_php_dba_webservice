<?php

use connection\ConnectionWrapper;

if($_SERVER["REQUEST_METHOD"] == "POST")
{
    require_once(__DIR__ . '/../src/service/RegisterService.php');
    require_once(__DIR__ . '/../src/validators/RegisterValidator.php');
    require_once(__DIR__ . '/../src/ConnectionWrapper.php');

    $jsonRequestParams = json_decode(file_get_contents('php://input'), true);

    $result = array();
    $result["errors"] = array();
    $result["registerResult"] = array();

    try
    {
        $connectionWrapper = new ConnectionWrapper();
        $registerValidator = new RegisterValidator();
        $registerService = new RegisterService($connectionWrapper->getConnection(), $registerValidator);

        $login = $jsonRequestParams["login"];
        $email = $jsonRequestParams["email"];
        $password1 = $jsonRequestParams["password1"];
        $password2 = $jsonRequestParams["password2"];
        $name = $jsonRequestParams["name"];
        $surname = $jsonRequestParams["surname"];
        $birthDate = $jsonRequestParams["birthDate"];

        $registerService->register($login, $email, $password1, $password2, $name, $surname, $birthDate);
        $registerResult = $registerService->getRegisterResult();
        if(!$registerResult)
            $result["errors"]["register"] = $registerService->getErrors();
        else
        {
            $result["registerResult"] = $registerService->getRegisterResult();
        }

    } catch (Exception $e)
    {
        $result["errors"]["connection"] = $e->getMessage();
    }
    echo json_encode($result);
}
?>
