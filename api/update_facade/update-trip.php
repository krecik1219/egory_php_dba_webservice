<?php
if($_SERVER["REQUEST_METHOD"] == "POST")
{
    require_once(__DIR__ . '/../../src/executors/Updater.php');

    $jsonRequestParams = json_decode(file_get_contents('php://input'), true);

    $tripId = (int)$jsonRequestParams["tripId"];
    $tripName = $jsonRequestParams["tripName"];

    $updater = new Updater();
    echo $updater->update("updateTrip", $tripId, $tripName);
}
?>
