<?php
if($_SERVER["REQUEST_METHOD"] == "POST")
{
    require_once(__DIR__ . '/../../src/executors/Updater.php');

    $jsonRequestParams = json_decode(file_get_contents('php://input'), true);

    $sessionId = (int)$jsonRequestParams["sessionId"];
    $sessionName = $jsonRequestParams["sessionName"];
    $endDatetime = $jsonRequestParams["endDatetime"];

    $updater = new Updater();
    echo $updater->update("updateTrackingSession", $sessionId, $sessionName, $endDatetime);
}
?>
