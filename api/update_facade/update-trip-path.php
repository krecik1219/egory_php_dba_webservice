<?php
if($_SERVER["REQUEST_METHOD"] == "POST")
{
    require_once(__DIR__ . '/../../src/executors/Updater.php');

    $jsonRequestParams = json_decode(file_get_contents('php://input'), true);

    $tripPathId = (int)$jsonRequestParams["tripPathId"];
    $isDone = (int)$jsonRequestParams["isDone"];

    $updater = new Updater();
    echo $updater->update("updateTripPath", $tripPathId, $isDone);
}
?>
