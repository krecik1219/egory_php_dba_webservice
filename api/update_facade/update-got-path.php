<?php
if($_SERVER["REQUEST_METHOD"] == "POST")
{
    require_once(__DIR__ . '/../../src/executors/Updater.php');

    $jsonRequestParams = json_decode(file_get_contents('php://input'), true);

    $pathId = (int)$jsonRequestParams["pathId"];
    $upPoints = (int)$jsonRequestParams["upPoints"];
    $downPoints = (int)$jsonRequestParams["downPoints"];

    $updater = new Updater();
    echo $updater->update("updateGotPath", $pathId, $upPoints, $downPoints);
}
?>
