<?php
if($_SERVER["REQUEST_METHOD"] == "POST")
{
    require_once(__DIR__ . '/../../src/executors/Deleter.php');

    $jsonRequestParams = json_decode(file_get_contents('php://input'), true);

    $tripPathId = (int)$jsonRequestParams["tripPathId"];

    $deleter = new Deleter();
    echo $deleter->delete("deleteTripPath", $tripPathId);
}
?>
