<?php
if($_SERVER["REQUEST_METHOD"] == "POST")
{
    require_once(__DIR__ . '/../../src/executors/Deleter.php');

    $jsonRequestParams = json_decode(file_get_contents('php://input'), true);

    $tripId = (int)$jsonRequestParams["tripId"];

    $deleter = new Deleter();
    echo $deleter->delete("deleteTrip", $tripId);
}
?>
