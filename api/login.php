<?php

if($_SERVER["REQUEST_METHOD"] == "POST")
{
    require_once(__DIR__ . '/../src/service/LoginService.php');
    require_once(__DIR__ . "/../src/ConnectionWrapper.php");
    require_once(__DIR__ . "/../src/validators/GeneralValidator.php");

    $jsonRequestParams = json_decode(file_get_contents('php://input'), true);

    $result = array();
    $result["errors"] = array();
    $result["loginResult"] = array();

    try
    {
        $connectionWrapper = new connection\ConnectionWrapper();
        $generalValidator = new GeneralValidator();
        $loginService = new LoginService($connectionWrapper->getConnection(), $generalValidator);

        $loginResult = $loginService->login($jsonRequestParams["login"], $jsonRequestParams["email"], $jsonRequestParams["password"]);
        if(!$loginResult)
            $result["errors"]["login"] = $loginService->getErrors();
        else
        {
            $result["loginResult"] = $loginService->getLoginResult();
        }

    } catch (Exception $e)
    {
        $result["errors"]["connection"] = $e->getMessage();
    }
    echo json_encode($result);
}
?>
