<?php
if($_SERVER["REQUEST_METHOD"] == "POST")
{
    require_once(__DIR__ . '/../../src/executors/ComplexExecutor.php');

    $jsonRequestParams = json_decode(file_get_contents('php://input'), true);

    $userId = (int)$jsonRequestParams["userId"];
    $sessions = $jsonRequestParams["sessions"];

    $complexExecutor = new ComplexExecutor();
    echo $complexExecutor->doComplexTask("syncTrackingSessions", $userId, $sessions);
}
?>
